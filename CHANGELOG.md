# Stuart-Redis Changelog

## [Unreleased]
### Changed
- [#2](https://gitlab.com/megalithic-llc/stuart-redis/-/issues/2) Use build matrix to simplify GitLab CI config
- [#1](https://gitlab.com/megalithic-llc/stuart-redis/-/issues/1) Standardize formatting with StyLua

## [0.2.1] - 2021-11-11
### Changed
- Migrated continuous integration automation to GitLab

## [0.2.0] - 2019-03-08
### Added
- RDD read-write support for string, hash, list, set, sorted set

## [0.1.0] - 2019-02-25
### Added
- A `PubSubReceiver` and test program is provided to make it easy to ingest Redis pub/sub messages from a Spark Streaming loop.
