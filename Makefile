.PHONY: lint
lint:
	luacheck --codes src spec examples

.PHONY: check
check:
	busted -v --defer-print
